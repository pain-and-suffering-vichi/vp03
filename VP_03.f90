﻿!  VP_03.f90 
!
!  FUNCTIONS:
!  VP_03 - Находит решение системы линейных уравнений
!

!****************************************************************************
!
!  PROGRAM: VP_03
!
!  PURPOSE:  Чтение и вывод данных, выбор метода
!
!****************************************************************************

    program VP_03
    use methods
    implicit none
    
    real(8), allocatable :: A(:,:), Extended(:,:), B(:), X(:)
    character(6) :: mettype
    integer(4) :: i, n, k

    open(1,file='data.dat')
    read(1,'(2x,I6)') n
        allocate(A(n,n),B(n),X(n),Extended(n+1,n))
    read(1,*) A
    read(1,*) B
    Extended(1:n,1:n)=A
    Extended(n+1,1:n)=B 
    close (1)

    open(2,file='result.dat', status='replace')
    close (2)
   
    call getarg(1,mettype)

    select case (mettype)
        case ('gauss')
            call gauss(extended,x,n)
            call output(n,X,mettype,A,B)
        case ('jordan')
            call jordan(extended,x,n)
            call output(n,X,mettype,A,B)
        case ('choise')
            call choise(extended,x,n)
            call output(n,X,mettype,A,B)
        case ('all')
            call gauss(extended,x,n)
            call output(n,X,mettype,A,B)
            call jordan(extended,x,n)
            call output(n,X,mettype,A,B)
            call choise(extended,x,n)
            call output(n,X,mettype,A,B)
        case default
            write(*,*) "all"
            mettype='all'
            write(*,*) 'Неверно указан ключ. &
            &По умолчанию будут выполнены все методы.'
            write(*,*) 'Для выбора метода гаусса используйте &
                &ключ gauss, для метода жордана jordan, &
                &для метода с выбором ведущего элемента choise' ! Это выглядит... грустно
            call gauss(extended,x,n)
            call output(n,X,mettype,A,B)
            call jordan(extended,x,n)
            call output(n,X,mettype,A,B)
            call choise(extended,x,n)
            call output(n,X,mettype,A,B)
        end select

    deallocate(A,B,Extended,X)
    end program VP_03
